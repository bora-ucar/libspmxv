#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "basics.h"


/*
sample runs 

mpirun -np 2 test1D a_mtx.txt a_mtx.txt_x_part_vec_2_c a_mtx.txt_y_part_vec_2_c 2

mpirun -np 2 test1D a_mtx.txt a_mtx.txt_x_part_vec_2_r a_mtx.txt_y_part_vec_2_r 3

will compute for ITER many items (ITER is a define constant below)

y = Ax.

If A is column-wise partitioned, a_mtx.txt_x_part_vec_2_c will define the partition on the
columns of A as well. In this case the last argument should be 2.


If A is rowwise partitioned, a_mtx.txt_y_part_vec_2_r will define the partition on the
rows of A as well. In this case the last argument should be 3.

The program does a basic check of its arguments (as of 12 oct 2019).

*/

#define ITER 10000

int main(int argc, char *argv[])
{

	int myId;
	int numProcs;
	int i;
	int *rowIndices, *colIndices;
	double *val;

	buMatrix *mtrx;
	buMatrix *loc;
	buMatrix *cpl;

	int partScheme;
	parMatrix *A;

	comm *in;
	comm *out;

	buVector *x, *b , *y, *bhat;

	double readStart, readEnd;
	double setupStart, setupEnd;
	double mxvStart, mxvEnd;

	double barrStart, barrEnd;

	double gReadTime, gSetupTime, gMxvTime, gBarrTime;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numProcs);
	MPI_Comm_rank(MPI_COMM_WORLD, &myId);


	mtrx  = (buMatrix*) malloc(sizeof(buMatrix));
	loc = (buMatrix *) malloc(sizeof(buMatrix));
	cpl = (buMatrix *) malloc(sizeof(buMatrix));

	in = allocComm();
	out = allocComm();


	initParLib(MPI_COMM_WORLD);

	partScheme = atoi(argv[4]);

	if(partScheme != PART_BY_ROWS && partScheme != PART_BY_COLUMNS){
		printf("PartScheme is noot ok\n");
		quitParLib(MPI_COMM_WORLD);
		MPI_Finalize();
		exit(1002);
	}

	
	/*		partScheme = PART_BY_ROWS;*/

	MPI_Barrier(MPI_COMM_WORLD);
	readStart = MPI_Wtime();   

	readMatrix(mtrx, argv[1],  partScheme, argv[2], argv[3] );      
	readEnd = MPI_Wtime() - readStart;
	MPI_Barrier(MPI_COMM_WORLD);
	setupStart = MPI_Wtime();

	setupMisG(partScheme, mtrx, loc, cpl, in, out, MPI_COMM_WORLD);
	setupEnd = MPI_Wtime() - setupStart;

	A = (parMatrix *) malloc(sizeof(parMatrix));
	A->loc = loc;
	A->cpl = cpl;
	A->in = in;
	A->out = out;
	A->scheme = partScheme;
	
	if(partScheme != PART_BY_COLUMNS)
	{
		if(A->loc->n - A->in->recv->all[numProcs])
		{
			x = allocVector(A->loc->n - A->in->recv->all[numProcs]);
			for( i  = 0 ; i < x->sz; i++)
				x->val[i] = myId+1;
		}
		else
		{
			printf("\nSomething wrong");
			MPI_Finalize();
			return 0;
		}
	}
	else
	{
		x = allocVector(A->loc->n);
		for( i  = 0 ; i < x->sz; i++)
			x->val[i] = myId+1;
	}

	b = (buVector *)malloc(sizeof(buVector));
	b->sz = 0;
	/*generate a rhs of A*1 = b;*/ 


	MPI_Barrier(MPI_COMM_WORLD);
	mxvStart = MPI_Wtime();

	for(i = 0; i < ITER; i++) 
	{
		int jb;
		for( jb = 0 ; jb < b->sz; jb++)
			b->val[jb] = 0.0;
		mxv(A, 
			x, 
			b, MPI_COMM_WORLD); 

	}
	MPI_Barrier(MPI_COMM_WORLD);
	mxvEnd = MPI_Wtime() - mxvStart;

	/*
MPI_Barrier(MPI_COMM_WORLD);
barrStart = MPI_Wtime();

for(i = 0; i < ITER; i++) 
{
int jb;
for( jb = 0 ; jb < b->sz; jb++)
b->val[jb] = 0.0;
MPI_Barrier(MPI_COMM_WORLD);
}
barrEnd = MPI_Wtime() - barrStart;   
*/

	MPI_Reduce(&readEnd, &gReadTime, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
	MPI_Reduce(&setupEnd, &gSetupTime, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
	MPI_Reduce(&mxvEnd, &gMxvTime, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);


	/* 

MPI_Reduce(&barrEnd, &gBarrTime, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);

*/


	if(myId == 0)
		printf("times: readtime=%lf(sec), setuptime=%lf(sec), mxvtime(msec)=%lf\n",
			gReadTime, gSetupTime, gMxvTime/(ITER/1000.0));


	/*
MPI_Barrier(MPI_COMM_WORLD);
if(myId==0)
{     
printf("\n\t****\tRight hand side generated 0###\t****");
printDblVec(b->val, b->sz);
}
MPI_Barrier(MPI_COMM_WORLD);
if(myId==1)
{     
	printf("\n\t****\tRight hand side generated 1###\t****");
	printDblVec(b->val, b->sz);
	} 
MPI_Barrier(MPI_COMM_WORLD);
if(myId==2)
	{     
	printf("\n\t****\tRight hand side generated 2###\t****");
	printDblVec(b->val, b->sz);
	} 
MPI_Barrier(MPI_COMM_WORLD);
if(myId==3)
	{     
	printf("\n\t****\tRight hand side generated 3###\t****");
	printDblVec(b->val, b->sz);
	}
*/ 
	freeMatrix(mtrx);
	freeMatrix(loc);
	freeMatrix(cpl); 
	free(A);
	freeVector(x);
	freeVector(b);
	freeComm(out); 
	freeComm(in);


	quitParLib(MPI_COMM_WORLD);




	MPI_Finalize();
	return 0;
}

void printSendRecvs(comm *in, int who)
{
	int myId;


	MPI_Comm_rank(MPI_COMM_WORLD, &myId);
	if(myId == who)
	{
		int i;
		printf("\n******************************\n");
		printf("\n I=%d will send %d",myId, in->send->num);
		for(i = 0 ; i<in->send->num; i++)
		{
			int p = in->send->lst[i];
			int sz = in->send->all[p+1] - in->send->all[p];
			int st = in->send->all[p];
			int j, js = st, je = in->send->all[p+1];
			printf("\n%d To==> %d", myId, p);
			for( j = js ; j < je; j++)
			{
				int ind = in->send->ind[j];
				printf("\nI %d to %d, ind %d, val %lf", myId, p, ind,in->send->val[j]);
			}
		}
		printf("\n I=%d will recv %d",myId, in->recv->num); 
		for(i = 0; i<in->recv->num; i++)
		{
			int p = in->recv->lst[i];
			int sz = in->recv->all[p+1] - in->recv->all[p]; 
			int st = in->recv->all[p];
			int ind = in->recv->ind[st];
			printf("\n I %d recv %d vol from %d, to %d", myId, sz, p, in->recv->ind[st]); 
		}
		printf("\n******************************\n");
	}     

	


	MPI_Barrier(MPI_COMM_WORLD);
	return;
}

