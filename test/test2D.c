#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "basics.h"


/*
sample run 

 mpirun -np 2 test2D a_mtx.txt a_mtx.txt_FG_IN_PART_2 a_mtx.txt_FG_OUT_PART_2 a_mtx.txt_FG_NZ_PART_2

with 
==================FILE a_mtx.txt==================
5 4 6
1 2 12.0
2 4 24.0
3 3 33.0
3 1 31.0
4 3 43.0
5 3 53.0
=================================================

=================FILE a_mtx.txt_FG_IN_PART_2 ====
0 
1
0
1
=================================================

=================FILE a_mtx.txt_FG_OUT_PART_2====
1
0
0
1
0
=================================================

=================FILE a_mtx.txt_FG_NZ_PART_2=====
0
1
0
1
1
0
=================================================


produces somethign like

***Matrix File readed. rows: 5 cols: 4 nonzeros: 6***
***Coords partvec is read***
***row col partvec is read***
***readMatrixCoordinates OK***
Comm (IN): TM MM TV MM 2 1 3 2
Comm (OUT): TM MM TV MM 2 1 3 2
times: readtime=0.013973(sec), setuptime=0.000760(sec), mxvtime(msec)=0.044762

*/

#define ITER 10000

int main(int argc, char *argv[])
{

	int myId;
	int numProcs;
	int i;
	int *rowIndices, *colIndices;
	double *val;

	buMatrix *mtrx;
	buMatrix *loc;
	buMatrix *cpl;

	int partScheme;
	parMatrix *A;

	comm *in;
	comm *out;

	buVector *x, *b , *y, *bhat;

	double readStart, readEnd;
	double setupStart, setupEnd;
	double mxvStart, mxvEnd;

	double barrStart, barrEnd;

	double gReadTime, gSetupTime, gMxvTime, gBarrTime;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numProcs);
	MPI_Comm_rank(MPI_COMM_WORLD, &myId);


	mtrx  = (buMatrix*) malloc(sizeof(buMatrix));
	loc = (buMatrix *) malloc(sizeof(buMatrix));
	cpl = (buMatrix *) malloc(sizeof(buMatrix));

	in = allocComm();
	out = allocComm();


	initParLib(MPI_COMM_WORLD);

	/* partScheme = atoi(argv[5]);*/

	partScheme = PART_2D;
	if((partScheme == PART_2D) || (partScheme == PART_CHK))
	{
		int commStat[4];
		int maxCommStat[4];
		int totCommStat[4];
		
		int postSendCounts[numProcs];
		int rankedList[numProcs];

		int tempSendList[numProcs];
		int newDest;

		MPI_Barrier(MPI_COMM_WORLD);
		readStart = MPI_Wtime();


		/*		sprintf(inPartName, "%s_FG_IN_PART_%s", argv[1], argv[2]);
		sprintf(outPartName, "%s_FG_OUT_PART_%s", argv[1], argv[2]);
		sprintf(nnzPartName, "%s_FG_NZ_PART_%s", argv[1], argv[2]);
		*/
		readMatrixCoordinates(&rowIndices, &colIndices, &val, &(mtrx->nnz), 
		&(mtrx->gm), &(mtrx->gn), &(mtrx->outPart), &(mtrx->inPart),
		argv[1], /*3coord*/
		argv[2], /*in part*/
		argv[3], /*out part*/
		argv[4], /*nnzpart*/
		MPI_COMM_WORLD, partScheme);
		
		
		readEnd = MPI_Wtime() - readStart;
		MPI_Barrier(MPI_COMM_WORLD);
		setupStart = MPI_Wtime();

		setup2D(rowIndices, colIndices, val, mtrx->nnz, /*coordinate format*/
		mtrx, loc, cpl,
		in, out,
		MPI_COMM_WORLD);
		
		setupEnd = MPI_Wtime() - setupStart;
		
		commStat[0] = in->send->num;
		commStat[1] = in->send->all[numProcs];
		commStat[2] = out->send->num;
		commStat[3] = out->send->all[numProcs];
		
		MPI_Reduce(commStat, maxCommStat, 4, MPI_INT, MPI_MAX, 0, MPI_COMM_WORLD);
		MPI_Reduce(commStat, totCommStat, 4, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

		if(myId == 0)
		{
			printf("Comm (IN): TM MM TV MM %d %d %d %d\n",totCommStat[0],maxCommStat[0],
			totCommStat[1], maxCommStat[1]);
			
			printf("Comm (OUT): TM MM TV MM %d %d %d %d\n",totCommStat[2],maxCommStat[2],
			totCommStat[3], maxCommStat[3]);
		}

	}/*2D case above*/


	A = (parMatrix *) malloc(sizeof(parMatrix));
	A->loc = loc;
	A->cpl = cpl;
	A->in = in;
	A->out = out;
	A->scheme = partScheme;
	
	if(partScheme != PART_BY_COLUMNS)
	{
		if(A->loc->n - A->in->recv->all[numProcs])
		{
			x = allocVector(A->loc->n - A->in->recv->all[numProcs]);
			for( i  = 0 ; i < x->sz; i++)
			  x->val[i] = myId+1;
		}
		else
		{
			printf("\nSomething wrong");
			MPI_Finalize();
			return 0;
		}
	}
	else
	{
		x = allocVector(A->loc->n);
		for( i  = 0 ; i < x->sz; i++)
		  x->val[i] = myId+1;
	}

	b = (buVector *)malloc(sizeof(buVector));
	b->sz = 0;
	/*generate a rhs of A*1 = b;*/ 


	MPI_Barrier(MPI_COMM_WORLD);
	mxvStart = MPI_Wtime();

	for(i = 0; i < ITER; i++) 
	{
		int jb;
		for( jb = 0 ; jb < b->sz; jb++)
		  b->val[jb] = 0.0;
		mxv(A, 
		    x, 
		    b, MPI_COMM_WORLD); 

	}
	MPI_Barrier(MPI_COMM_WORLD);
	mxvEnd = MPI_Wtime() - mxvStart;

	/*
MPI_Barrier(MPI_COMM_WORLD);
barrStart = MPI_Wtime();

for(i = 0; i < ITER; i++) 
{
int jb;
for( jb = 0 ; jb < b->sz; jb++)
b->val[jb] = 0.0;
MPI_Barrier(MPI_COMM_WORLD);
}
barrEnd = MPI_Wtime() - barrStart;   
*/

	MPI_Reduce(&readEnd, &gReadTime, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
	MPI_Reduce(&setupEnd, &gSetupTime, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
	MPI_Reduce(&mxvEnd, &gMxvTime, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);


	/* 

MPI_Reduce(&barrEnd, &gBarrTime, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);

*/


	if(myId == 0)
	  printf("times: readtime=%lf(sec), setuptime=%lf(sec), mxvtime(msec)=%lf\n",
		 gReadTime, gSetupTime, gMxvTime/(ITER/1000.0));


	/*
MPI_Barrier(MPI_COMM_WORLD);
if(myId==0)
{     
printf("\n\t****\tRight hand side generated 0###\t****");
printDblVec(b->val, b->sz);
}
MPI_Barrier(MPI_COMM_WORLD);
if(myId==1)
{     
	printf("\n\t****\tRight hand side generated 1###\t****");
	printDblVec(b->val, b->sz);
	} 
MPI_Barrier(MPI_COMM_WORLD);
if(myId==2)
	{     
	printf("\n\t****\tRight hand side generated 2###\t****");
	printDblVec(b->val, b->sz);
	} 
MPI_Barrier(MPI_COMM_WORLD);
if(myId==3)
	{     
	printf("\n\t****\tRight hand side generated 3###\t****");
	printDblVec(b->val, b->sz);
	}
*/ 
	freeMatrix(mtrx);
	freeMatrix(loc);
	freeMatrix(cpl); 
	free(A);
	freeVector(x);
	freeVector(b);
	freeComm(out); 
	freeComm(in);


	quitParLib(MPI_COMM_WORLD);




	MPI_Finalize();
	return 0;
}

void printSendRecvs(comm *in, int who)
{
	int myId;


	MPI_Comm_rank(MPI_COMM_WORLD, &myId);
	if(myId == who)
	{
		int i;
		printf("\n******************************\n");
		printf("\n I=%d will send %d",myId, in->send->num);
		for(i = 0 ; i<in->send->num; i++)
		{
			int p = in->send->lst[i];
			int sz = in->send->all[p+1] - in->send->all[p];
			int st = in->send->all[p];
			int j, js = st, je = in->send->all[p+1];
			printf("\n%d To==> %d", myId, p);
			for( j = js ; j < je; j++)
			{
				int ind = in->send->ind[j];
				printf("\nI %d to %d, ind %d, val %lf", myId, p, ind,in->send->val[j]);
			}
		}
		printf("\n I=%d will recv %d",myId, in->recv->num); 
		for(i = 0; i<in->recv->num; i++)
		{
			int p = in->recv->lst[i];
			int sz = in->recv->all[p+1] - in->recv->all[p]; 
			int st = in->recv->all[p];
			int ind = in->recv->ind[st];
			printf("\n I %d recv %d vol from %d, to %d", myId, sz, p, in->recv->ind[st]); 
		}
		printf("\n******************************\n");
	}     

	


	MPI_Barrier(MPI_COMM_WORLD);
	return;
}

