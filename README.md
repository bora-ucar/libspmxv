# LibSpMxV: An MPI-based library for sparse matrix--vector multiplication

This repository contains a linbrary for implementing sparse matrix--vector
multiplies with MPI. There are no external dependecies. Support functions
are included for basic operations.


The core design is decsribed in 

**Bora Uçar and Cevdet Aykanat**. *A Library for Parallel Sparse Matrix Vector Multiplies*,
Tech Rep, [BU-CE-0506](http://perso.ens-lyon.fr/bora.ucar/papers/UcarAykanatParSpMxv.pdf), 
Bilkent University, Ankara Turkey, 2003.

It is updated and analysed in

**Kamer Kaya, Bora Uçar, and Ümit V. Çatalyürek**. *Analysis of Partitioning Models and Metrics in Parallel Sparse Matrix-Vector Multiplication*, 
in Proc. Parallel Processing and Applied Mathematics (PPAM 2013), Warsaw, Poland, Sep. 2013, whose extended version is 
available as a technical report available at [HAL](https://hal.inria.fr/hal-00821523/file/RR-8301.pdf).

<pre>	
@inproceedings{kauc:14c,
	Address = {Warsaw, Poland},
	Author = {Kaya, Kamer and U\c{c}ar, Bora and \c{C}ataly\"{u}rek, \"{U}mit V.},
	Booktitle = {Parallel Processing and Applied Mathematics (PPAM, Sep. 2013)},
	Editor = {Wyrzykowski, Roman and Dongarra, Jack and Karczewski, Konrad and Wa{\'s}niewski, Jerzy},
	Pages = {174--184},
	Publisher = {Springer Berlin Heidelberg},
	Series = {Lecture Notes in Computer Science},
	Title = {Analysis of Partitioning Models and Metrics in Parallel Sparse Matrix-Vector Multiplication},
	Year = {2014}
}

@techreport{catalyurek:hal-00821523,
  TITLE = {{On analysis of partitioning models and metrics in parallel sparse matrix-vector multiplication}},
  AUTHOR = {\c{C}ataly\"urek, \"{U}mit V. and Kaya, Kamer and U{\c c}ar, Bora},
  URL = {https://hal.inria.fr/hal-00821523},
  TYPE = {Research Report},
  NUMBER = {RR-8301},
  PAGES = {25},
  INSTITUTION = {{INRIA}},
  YEAR = {2013},
  MONTH = May,
  KEYWORDS = {hypergraph partitioning ; Parallel sparse-matrix vector multiplication},
  PDF = {https://hal.inria.fr/hal-00821523/file/RR-8301.pdf},
  HAL_ID = {hal-00821523},
  HAL_VERSION = {v3},
}
</pre>

## License
See the file LICENSE. The software is under CeCILL-B license, which is a BSD-like license.

## Contact
The e-mail addresses of the authors are:
umit@gatech.edu, kamer.kaya@sabanciuniv.edu, and bora.ucar@ens-lyon.fr.

## Build
There are two subdirectories for the codes.
`libsrc`: contains the source files of the library. The makefile here 
uses `mpicc` to build the library.
`test`: contains two drivers for testing the library. The makefile here
uses `mpicc` to build the executables.
Assuming the availability of mpicc with the paths, `make` should build a library and 
the executables `test/test1D test/test2D`.

## Usage and arguments
In y=Ax, we call x input vector, and y output vector.

In the files, part numbers are from 0 to k-1. Matrix indices are from 1. 

Which file to use for SpMxV:

+  If you have 1D partition: Meaning that either the partition on the input
vector defines the partition on the columns of A, or the partition on the output 
vector defines the partition on the rows of A, then `test1D.c` should be used.

`mpirun -np 2 test1D a_mtx.txt a_mtx.txt_x_part_vec_2_c a_mtx.txt_y_part_vec_2_c 2`

`mpirun -np 2 test1D a_mtx.txt a_mtx.txt_x_part_vec_2_r a_mtx.txt_y_part_vec_2_r 3`

will compute for ITER many items (ITER is a constant in test1D.c) y = Ax.

If A is column-wise partitioned, `a_mtx.txt_x_part_vec_2_c` will define the partition on the
columns of A as well. In this case the last argument should be `2`.

If A is rowwise partitioned, `a_mtx.txt_y_part_vec_2_r` will define the partition on the
rows of A as well. In this case the last argument should be `3`.


+ Anything other partition: Then `test2D.c` should be useful. This is written for any sort of partition.
One specifies the partition on the nonzeros of A individually, on the input vector
and on the output vector.

`mpirun -np 2 test2D a_mtx.txt a_mtx.txt_FG_IN_PART_2 a_mtx.txt_FG_OUT_PART_2 a_mtx.txt_FG_NZ_PART_2`



## Files
+ LICENSE: Describes the CeCILL-B license.
+ make.inc: Defines the directories
+ README.md: This file.
+ incs/basics.h: A header file defining the basic types, structs, and constants.
+ libsrc/buVector.c: Implements the vector data structure.
+ libsrc/mxv.c: Implements the sparse matrix--vector multiplication algorithms.
+ libsrc/matrix.c: Implements the matrix data structure.		
+ libsrc/setup.c: Implements support routines for setting up communication to be used in the multiplication routines.
+ test/test1D.c: The use case of the library with 1D partitions.
+ test/test2D.c: The use case of the library with 2D (arbitrary) partitions.
+ test/a_mtx.txt: A sample matrix.
+ test/a_mtx.txt_x_part_vec_2_c: A sample 2-way input vector partition (for 1D column-parallel SpMxV).
+ test/a_mtx.txt_x_part_vec_2_r: A sample 2-way input vector partition (for 1D row-parallel SpMxV).
+ test/a_mtx.txt_y_part_vec_2_c: A sample 2-way output vector partition (for 1D column-parallel SpMxV).
+ test/a_mtx.txt_y_part_vec_2_r: A sample 2-way outpt vector partition (for 1D row-parallel SpMxV).
+ test/a_mtx.txt_FG_IN_PART_2: A sample 2-way input vector partition (for 2D row-column-parallel SpMxV).
+ test/a_mtx.txt_FG_OUT_PART_2:	A sample 2-way output vector partition (for 2D row-column-parallel SpMxV).
+ test/a_mtx.txt_FG_NZ_PART_2: A sample 2-way nonzero partition of the sample matrix (for 2D row-column-parallel SpMxV).

## Versions
This is version v2.0 (December 2019). Earlier version (2003) had now-obselete MPI calls.
