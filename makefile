include make.inc
DIRS = libsrc test

default:
	@for i in $(DIRS); do        \
		cd $$i;               \
		$(MAKE) $@ || exit 1; \
	        cd ..;		    \
	done
depend:
	@for i in $(DIRS); do                  \
		cd $$i;                            \
		$(MAKE) $@ || exit 1;              \
		cd ..;                             \
	done

clean:
	@for i in $(DIRS); do                  \
		cd $$i;                            \
		$(MAKE) $@ || exit 1;              \
		cd ..;                             \
	done
